# 🎲 Yathzee 🎲
## Pré-requis 🔨
Avoir Python: https://www.python.org/downloads/
## Setup le projet ⚙️

`git clone git@gitlab.com:Totiii/yathzee.git`

`pip install -r requirement.pip`

## Lancer le serveur 🚀

`py manage.py runserver`

Aller sur: http://127.0.0.1:8000/

## Lancer les tests ✔️

`py manage.py test`
