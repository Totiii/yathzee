from django.test import TestCase


class YahtzeeTest(TestCase):
    # def setUp(self):

    def test_get_dice_numbers_with_dice_from_1_to_5(self):
        from .views import get_dice_numbers

        self.assertEqual({
             '1': 1,
             '2': 2,
             '3': 3,
             '4': 4,
             '5': 5,
         }, get_dice_numbers([1, 2, 3, 4, 5]))

    def test_get_dice_numbers_with_3_dice_of_1_and_2_of_4(self):
        from .views import get_dice_numbers

        self.assertEqual({
            '1': 3,
            '4': 8,
        }, get_dice_numbers([1, 1, 1, 4, 4]))

    def test_get_dice_count_true_with_3(self):
        from .views import get_dice_count

        self.assertEqual(True, get_dice_count([1, 1, 1, 3, 4], 3))

    def test_get_dice_count_true_with_3_and_5_same_numbers(self):
        from .views import get_dice_count

        self.assertEqual(True, get_dice_count([1, 1, 1, 1, 1], 3))

    def test_get_dice_count_true_with_3_and_5_same_numbers_and_strict(self):
        from .views import get_dice_count

        self.assertEqual(False, get_dice_count([1, 1, 1, 1, 1], 3, True))

    def test_get_dice_count_false_with_3(self):
        from .views import get_dice_count

        self.assertEqual(False, get_dice_count([1, 1, 2, 3, 4], 3))

    def test_get_dice_count_true_with_4(self):
        from .views import get_dice_count

        self.assertEqual(True, get_dice_count([1, 1, 1, 1, 4], 4))

    def test_get_dice_count_false_with_4(self):
        from .views import get_dice_count

        self.assertEqual(False, get_dice_count([1, 1, 1, 3, 4], 4))

    def test_get_dice_count_with_5_same_number_return_true(self):
        from .views import get_dice_count

        self.assertEqual(True, get_dice_count([1, 1, 1, 1, 1], 5))

    def test_get_three_of_a_kind_return_total(self):
        from .views import get_three_of_a_kind

        self.assertEqual(14, get_three_of_a_kind([3, 3, 3, 1, 4]))

    def test_get_three_of_a_kind_return_total_with_yahtzee(self):
        from .views import get_three_of_a_kind

        self.assertEqual(15, get_three_of_a_kind([3, 3, 3, 3, 3]))

    def test_get_three_of_a_kind_return_none(self):
        from .views import get_three_of_a_kind

        self.assertEqual(None, get_three_of_a_kind([3, 2, 3, 1, 4]))

    def test_get_four_of_a_kind_return_total(self):
        from .views import get_four_of_a_kind

        self.assertEqual(13, get_four_of_a_kind([3, 3, 3, 1, 3]))

    def test_get_four_of_a_kind_return_total_with_yahtzee(self):
        from .views import get_four_of_a_kind

        self.assertEqual(15, get_four_of_a_kind([3, 3, 3, 3, 3]))

    def test_get_four_of_a_kind_return_none(self):
        from .views import get_four_of_a_kind

        self.assertEqual(None, get_four_of_a_kind([3, 2, 3, 1, 4]))

    def test_get_full_house_return_25(self):
        from .views import get_full_house

        self.assertEqual(25, get_full_house([1, 1, 2, 2, 2]))

    def test_get_full_house_return_None(self):
        from .views import get_full_house

        self.assertEqual(None, get_full_house([1, 1, 1, 3, 4]))

    def test_check_followed_number_4_return_true(self):
        from .views import check_followed_numbers

        self.assertEqual(True, check_followed_numbers([1, 2, 3, 3, 4], 4))

    def test_check_followed_number_4_return_true_with_dice_rolled_randomly(self):
        from .views import check_followed_numbers

        self.assertEqual(True, check_followed_numbers([3, 2, 1, 3, 4], 4))

    def test_check_followed_number_4_return_false(self):
        from .views import check_followed_numbers

        self.assertEqual(False, check_followed_numbers([1, 2, 3, 5, 5], 4))

    def test_check_followed_number_5_return_true(self):
        from .views import check_followed_numbers

        self.assertEqual(True, check_followed_numbers([1, 2, 3, 5, 4], 5))

    def test_check_followed_number_5_return_false(self):
        from .views import check_followed_numbers

        self.assertEqual(False, check_followed_numbers([1, 2, 3, 4, 6], 5))

    def test_get_small_straight_return_30(self):
        from .views import get_small_straight

        self.assertEqual(30, get_small_straight([1, 2, 3, 3, 4]))

    def test_get_small_straight_return_30_with_dice_rolled_randomly(self):
        from .views import get_small_straight

        self.assertEqual(30, get_small_straight([3, 2, 1, 3, 4]))

    def test_get_small_straight_return_30_with_dice_rolled_randomly_and_a_large_straight(self):
        from .views import get_small_straight

        self.assertEqual(30, get_small_straight([3, 2, 1, 5, 4]))

    def test_get_small_straight_return_None(self):
        from .views import get_small_straight

        self.assertEqual(None, get_small_straight([1, 2, 3, 5, 5]))

    def test_get_large_straight_return_40(self):
        from .views import get_large_straight

        self.assertEqual(40, get_large_straight([1, 2, 3, 4, 5]))

    def test_get_large_straight_return_None(self):
        from .views import get_large_straight

        self.assertEqual(None, get_large_straight([1, 2, 3, 4, 4]))

    def test_get_chance(self):
        from .views import get_chance

        self.assertEqual(13, get_chance([1, 2, 2, 4, 4]))

    def test_get_yahtzee_return_50(self):
        from .views import get_yahtzee

        self.assertEqual(50, get_yahtzee([1, 1, 1, 1, 1]))

    def test_get_multiple_yahtzee_return_150(self):
        from .views import get_yahtzee

        self.assertEqual(150, get_yahtzee([1, 1, 1, 1, 1], True))

    def test_get_yahtzee_return_None(self):
        from .views import get_yahtzee

        self.assertEqual(None, get_yahtzee([1, 1, 2, 1, 1]))

    def test_game_with_yathzee(self):
        from .views import get_dice_numbers, get_three_of_a_kind, get_four_of_a_kind, get_full_house, get_small_straight, get_large_straight, get_chance, get_yahtzee

        roll = [1, 1, 1, 1, 1]
        self.assertEqual({
            "numbers": {
                "1": 5
            },
            "three_of_a_kind": 5,
            "four_of_a_kind": 5,
            "full_house": None,
            "small_straight": None,
            "large_straight": None,
            "chance": 5,
            "yahtzee": 50
        }, {
            "numbers": get_dice_numbers(roll),
            "three_of_a_kind": get_three_of_a_kind(roll),
            "four_of_a_kind": get_four_of_a_kind(roll),
            "full_house": get_full_house(roll),
            "small_straight": get_small_straight(roll),
            "large_straight": get_large_straight(roll),
            "chance": get_chance(roll),
            "yahtzee": get_yahtzee(roll)
        })
