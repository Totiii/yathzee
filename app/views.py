import random

from django.http import JsonResponse
from django.shortcuts import render


def get_dice_numbers(dice):
    result = {}
    for d in dice:
        try:
            result[str(d)] += d
        except KeyError:
            result[str(d)] = d
    return result


def get_dice_count(dice, number, strict=False):
    for d in dice:
        if strict:
            if dice.count(d) == number:
                return True
        else:
            if dice.count(d) >= number:
                return True
    return False


def get_three_of_a_kind(dice):
    if get_dice_count(dice, 3):
        return sum(dice)
    return None


def get_full_house(dice):
    if get_dice_count(dice, 3, True) and get_dice_count(dice, 2, True):
        return 25
    return None


def get_chance(dice):
    return sum(dice)


def check_followed_numbers(dice, check_range):
    for d in dice:
        if set(range(d, d+check_range)).issubset(set(dice)):
            return True
    return False


def get_small_straight(dice):
    if check_followed_numbers(dice, 4):
        return 30
    else:
        return None


def get_large_straight(dice):
    if check_followed_numbers(dice, 5):
        return 40
    else:
        return None


def get_yahtzee(dice, multiple=False):
    if get_dice_count(dice, 5) and multiple:
        return 150
    elif get_dice_count(dice, 5):
        return 50
    return None


def get_four_of_a_kind(dice):
    if get_dice_count(dice, 4):
        return sum(dice)
    return None


def roll_dice(nb):
    return_list = []
    for i in range(0, nb):
        return_list.append(random.randint(1, 6))
    return return_list


def index(request):
    roll = roll_dice(5)
    return JsonResponse(
        {
            "numbers": get_dice_numbers(roll),
            "three_of_a_kind": get_three_of_a_kind(roll),
            "four_of_a_kind": get_four_of_a_kind(roll),
            "full_house": get_full_house(roll),
            "small_straight": get_small_straight(roll),
            "large_straight": get_large_straight(roll),
            "chance": get_chance(roll),
            "yahtzee": get_yahtzee(roll)
        }
    )
